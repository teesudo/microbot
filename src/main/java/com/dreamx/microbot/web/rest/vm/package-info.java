/**
 * View Models used by Spring MVC REST controllers.
 */
package com.dreamx.microbot.web.rest.vm;
